
This is s2i image that will build sphinx documentation from a git repository. It is intended to build and documentation for ClusterQA projects and expose it in Openshift. So far this should work for [sts](http://git.app.eng.bos.redhat.com/git/clusterqe/sts.git/) and for [ansible-roles](https://gitlab.cee.redhat.com/mnovacek/ansible-roles/)

Based on Openshift [blog](https://blog.openshift.com/create-s2i-builder-image/).

Everytime this repository is updated in a way that the resulting image changes
it is necessary to manually build (`make build`) and import it to openshift
(`make import`).

The other option is to crete build config using [openshift.bc.yml](openshift.bc.yml) which can leave rebuilding of the image on Openshift. In that case you need to poke Openshift instance into rebuilding using either `oc start-build` or do that in web ui.

## Example of real world usage with a project in Openshift

### Prerequisities

 * installed `oc` app
 * image from this repository pushed to Openshift accessible registry
 * project on git that have `documentation/` directory where running `make doc` will create `build/html` directory with documentation

### (Optional) Check with s2i

* Install ``s2i`` from [RHCSL redhat repository](http://download-ipv4.eng.brq.redhat.com/released/RHSCL/3.0/RHEL-7/Server/x86_64/os/).

* Build ``cqe-docker-doc`` image with static content from git repository not-master branch and from another than root directory using ``s2i-sphinx-doc`` builder image.
```
s2i build \
    --ref=feature-configure-py-3 \
    --context-dir=docker/ \
    https://gitlab.cee.redhat.com/mnovacek/cqe.git \
    docker-registry.engineering.redhat.com/cluster-qa/s2i-sphinx-doc \
    cqe-docker-doc
```

### Create new app with this builder image.
We need `GIT_SSL_NO_VERIFY` so git clone will not complain about missing certificates authorities.

```
$ DOCKER_IMAGE=
$ GIT=https://gitlab.cee.redhat.com/mnovacek/sts.git
$ oc new-app ${DOCKER_IMAGE}~${GIT}#RHEL7 \
    --name sts-rhel7-doc \
    --build-env=GIT_SSL_NO_VERIFY=true
```

#### It might be necessary to import the image to the imagestream.
```
$ DOCKER_IMAGE=docker-registry.engineering.redhat.com/cluster-qa/s2i-sphinx-doc
$ oc import-image ${DOCKER_IMAGE} --confirm --insecure=true
```

#### Readiness and liveness probes.

```
$ oc set probe dc/sts-rhel7-doc --readiness --get-url=http://:12121
$ oc set probe dc/sts-rhel7-doc --liveness -- echo ok
```
#### Expose route.
```
$ oc expose svc/sts-rhel7-doc \
    --hostname=sts-rhel7-doc.int.open.paas.redhat.com
```

Optional: Create ssl route edge.
```
$ oc create route edge \
    --key=ssl-dummy-keys/key.pem \
    --cert=ssl-dummy-keys/cert.pem \
    --ca-cert=ssl-dummy-keys/cert.pem \
    --service=znc \
    --hostname=sts-rhel7-doc.int.open.paas.redhat.com
```
### Create gitlab ci call to openshift api.

For the rebuild to happen automagicall you need to edit Gitlab CI yaml for the
project and add call to openshift api to trigger new build ([Openshift documentation](https://docs.openshift.com/container-platform/3.7/dev_guide/builds/triggering_builds.html#gitlab-webhooks))


