
# sphinx-doc
FROM openshift/base-centos7
# FROM registry.access.redhat.com/rhscl/httpd-24-rhel7
# FROM centos:7

ARG LABEL_BUILD_DATE

# TODO:there-should-be-openshift-url-here
ARG LABEL_URL

ARG LABEL_VCS_TYPE=git
ARG LABEL_VCS_URL=https://gitlab.cee.redhat.com/clusterqe/s2i-sphinx-build
ARG LABEL_VCS_REF=master
ARG LABEL_VERSION

ARG PACKAGES="\
lighttpd \
make \
python-sphinx \
python2-pip \
pandoc \
"
ARG PIP_PACKAGES="\
yaml2rst \
"
ARG APP_ROOT=/opt/app-root
ARG RUN_AS_USER=5001

LABEL \
    io.k8s.description="Platform for building sphinx documentation for Openshift" \
    io.k8s.display-name="Sphinx documentation builder." \
    io.openshift.expose-services="8080:http" \
    io.openshift.tags="s2i-sphinx-build" \
    com.redhat.cluster-qa.s2i-sphinx-build.build_date="$LABEL_BUILD_DATE" \
    com.redhat.cluster-qa.s2i-sphinx-build.name="Sphinx s2i build image." \
    com.redhat.cluster-qa.s2i-sphinx-build.summary="This is image for s2i \
that will build documentation for a project using 'make doc' run in \
documentation/ directory of the project. The resulting html will be copied \
as static content to the directory that lighttpd serves." \
    com.redhat.cluster-qa.s2i-sphinx-build.url="$LABEL_URL" \
    com.redhat.cluster-qa.s2i-sphinx-build.vcs-type="$LABEL_VCS_TYPE" \
    com.redhat.cluster-qa.s2i-sphinx-build.vcs-url="$LABEL_VCS_URL" \
    com.redhat.cluster-qa.s2i-sphinx-build.vcs-ref="$LABEL_VCS_REF" \
    com.redhat.cluster-qa.s2i-sphinx-build.version="$LABEL_VERSION" \
    maintainer="Michal Nováček <mnovacek@redhat.com>"

# add epel and its gpg key
RUN \
    rpm --import https://getfedora.org/static/352C64E5.txt && \
    yum-config-manager --add https://download.fedoraproject.org/pub/epel/7/x86_64/ && \
    yum -y install epel-release && \
    yum makecache && \
    yum install -y ${PACKAGES} && \
    yum clean all -y && \
    pip install --upgrade pip && \
    pip install ${PIP_PACKAGES}

COPY .s2i/bin/ /usr/libexec/s2i

COPY root/ /
RUN chown -R ${RUN_AS_USER}:${RUN_AS_USER} ${APP_ROOT}
USER ${RUN_AS_USER}

EXPOSE 8080

CMD ["/usr/libexec/s2i/usage"]
# ENTRYPOINT ["/bin/bash"]

