
LABEL_BUILD_DATE := $(shell date --rfc-3339=seconds)

IMAGE_REGISTRY ?= docker-registry.engineering.redhat.com
IMAGE_NAME ?= $(IMAGE_REGISTRY)/cluster-qa/s2i-sphinx-doc

HAVE_GIT := $(shell git --version 2>/dev/null)
ifdef HAVE_GIT
LAST_COMMIT := $(shell git rev-parse --short HEAD)
LAST_COMMIT_UNIX_TIMESTAMP := $(shell git show -s --format=%ct $(LAST_COMMIT))
LAST_COMMIT_DATE := $(shell date --date=@$(LAST_COMMIT_UNIX_TIMESTAMP) +%Y%m%d%H%M%S)
else
LAST_COMMIT := unknown-commit
endif

LABEL_BUILD_DATE := $(shell date --rfc-3339=seconds)
LABEL_VERSION := $(LAST_COMMIT_DATE)-$(LAST_COMMIT)

BUILD_ENV :=
BUILD_ENV += --build-arg LABEL_BUILD_DATE="$(LABEL_BUILD_DATE)"
BUILD_ENV += --build-arg LABEL_VERSION="$(LABEL_VERSION)"

build:
	docker build $(BUILD_ENV) -t $(IMAGE_NAME) .

push:
	docker push $(IMAGE_NAME)

import: push
	oc import-image $(IMAGE_NAME) --confirm --insecure=true

test:
	docker build -t $(IMAGE_NAME)-candidate .
	s2i build test/test-app $(IMAGE_NAME)-candidate test-app
	#docker run --rm -d -p 8080:8080 --name test-app test-app
	#sleep 5
	#test $(shell curl -s -o /dev/null -w "%{http_code}" http://localhost:8080/) == 200
	#curl -q http://localhost:8080 | 'grep Welcome to Test doc for build for sphinx-doc image.'
	#docker stop test-app
.PHONY: test
